# Frog Game
Simple Frog game made using Python3 and Pygame library.

## Documentation

| Type     | Link                                                        |
| -------- | ----------------------------------------------------------- |
| Docs     | https://www.pygame.org/docs/                                |
| Tutorial | https://pythonprogramming.net/pygame-python-3-part-1-intro/ |
| Tutorial | https://realpython.com/pygame-a-primer/                     |

Pygame is a python game development lib, mainly for 2d games. Sprites management oriented and based in SDL 1.2 lib.

## Setup

```bash
virtualenv frogger
cd frogger/
source bin/activate
```

### Install Pygame

```bash
pip install pygame
```

You can verify the install by loading one of the examples that comes with the library:

```bash
python -m pygame.examples.aliens
```

### See installed dependences

```bash
pip freeze

pygame==1.9.6
```

### Export dependences

```bash
pip freeze > requirements.txt
```

Install dependences from a `requirements.txt` file.

```bash
pip install -r requirements.txt
```

## Basic Window

Create file `main.py`

```bash
touch main.py
```

File `main.py`

```python
# -*- coding: utf-8 -*-
import pygame,sys # Pygame and sys modules
from pygame.locals import * # Import Pygame constants

pygame.init() # Setup pygame module
screen = pygame.display.set_mode((800,600)) # Creating a new window
pygame.display.set_caption("Hello Pygame!!!") # Window title

# Main loop
while True:
    # Iterate all events
    for event  in pygame.event.get():
        # If a event send the close window signal
        if event.type == QUIT:
            pygame.quit() # Closes pygame module
            sys.exit(0) # Finish the application
```

Execute app

```bash
python main.py
```

## Frog model

Create file `frog.py`

```bash
touch frog.py
```

File `frog.py`

```python
# -*- coding: utf-8 -*-
import pygame, sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants


# Create Frog class and extends from a Sprite
class Frog(pygame.sprite.Sprite):
    # Class constructor
    # :param x: horizontal position
    # :param y: vertical position
    # :param screen:
    def __init__(self, x, y, screen):
        pygame.sprite.Sprite.__init__(self)
        self.size = 16  # Sprite size
        self.x = x
        self.y = y
        self.score = 0  # Player score
        self.level = 1  # Game level
        self.screen = screen
        self.lastkey = None
        self.rect = pygame.Rect(self.x, self.y, self.size, self.size)

    # Draw the sprite in the screen
    def paint(self):
        pygame.draw.rect(self.screen, (244, 67, 54), self.rect)

    # Changes the sprite position
    def move(self, timer, keys, walls):
        if keys[K_RIGHT]:
            self.rect.x += 0.5 * timer
            if self.rect.x + self.size > 400:
                self.rect.x = 400 - self.size
        elif keys[K_LEFT]:
            self.rect.x -= 0.5 * timer
            if self.rect.x < 0:
                self.rect.x = 0
        elif keys[K_UP]:
            if (self.lastkey != K_UP):
                self.rect.y -= self.size * 2
                if (self.rect.y <= 0):
                    self.rect.y = self.size
            self.lastkey = K_UP

        elif keys[K_DOWN]:
            if (self.lastkey != K_DOWN):
                self.rect.y += self.size * 2
                if self.rect.y + self.size > 640:
                    self.rect.y = 640 - self.size
            self.lastkey = K_DOWN

        self.paint()

    # Verify boundaries intersections
    def intersects(self, targets):
        val = False
        for i in targets:
            if pygame.sprite.collide_rect(self, i):
                print("Collision !!!!!!!!!!!!!!!!!!")
                val = True
        return val
```

## Car model

Create file `car.py`

```bash
touch frog.py
```

File `car.py`

```python
# -*- coding: utf-8 -*-
import pygame
import sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants


# Create Car class and extends from a Sprite
class Car(pygame.sprite.Sprite):
    # Class constructor
    # :param x: horizontal position
    # :param y: vertical position
    # :param n:
    # :param screen:
    def __init__(self, x, y, n, screen):
        pygame.sprite.Sprite.__init__(self)  # Super method
        self.n = n
        self.speed = 0.05  # Car speed
        self.size = 16  # Sprite size
        self.x = x
        self.y = y
        self.screen = screen  # Canvas or screen
        self.rect = pygame.Rect(self.x, self.y, self.size, self.size)  # Boundaries

    # Draw the sprite in the screen
    def paint(self):
        pygame.draw.rect(self.screen, (61, 90, 254), self.rect)

    # Changes the sprite position
    def move(self, time):
        if (self.n % 2 == 0):
            self.rect.x += self.speed * time
            if (self.rect.x + self.size > 400):
                self.rect.x = 0
            self.paint()
        else:
            self.rect.x -= self.speed * time
            if (self.rect.x <= 0):
                self.rect.x = 400
            self.paint()
```

## Main window

File `main.py`

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants
from frog import Frog
from car import Car

# Constants
WIDTH = 400
HEIGHT = 640
CENTER = (WIDTH / 2) - (16 / 2)

BACKGROUND_COLOR = (255, 249, 196)
walls = x = [j for j in range(0, HEIGHT, 32)]


# Draw walls in the screen
def draw_walls(screen):
    for i in walls:
        rect = pygame.Rect(0, i, WIDTH, 16)
        pygame.draw.rect(screen, (0, 0, 0), rect)


# Draw cars in the screen
def draw_cars(cars):
    for i in cars:
        i.paint()


# Draw game info
def draw_score(screen, player):
    myfont = pygame.font.SysFont("monospace", 10)
    label = myfont.render("Score : " + str(player.score) +
                          " Level : " + str(player.level), 1, (255, 255, 0))
    screen.blit(label, (0, 0))


def main():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))  # Creating a new window
    pygame.display.set_caption("Hello Pygame!!!")  # Window title

    # Initialize vars
    player = Frog(CENTER, HEIGHT - 16, screen)
    clock = pygame.time.Clock()
    pygame.font.init()

    cars = []
    for i in range(1, 18):
        print("Creatinf car => ", i)
        car = Car(CENTER, walls[i] + player.size, i, screen)
        cars.append(car)

    # Game logic
    game_over = False
    # Main loop
    while True:
        time = clock.tick(15)

        keys = pygame.key.get_pressed()
        # Iterate all events
        for event in pygame.event.get():
            # If a event send the close window signal
            if event.type == QUIT:
                pygame.quit()  # Closes pygame module
                sys.exit(0)  # Finish the application

        # Background
        screen.fill(BACKGROUND_COLOR)

        # Draw elements
        player.move(time, keys, walls)
        draw_walls(screen)
        draw_score(screen, player)

        # Movements
        for i in cars:
            i.move(time)

        if (player.rect.y <= player.size):
            for i in cars:
                i.speed += 0.05
            player.score += 5
            player.level += 1
            player.rect.y = HEIGHT - player.size

        # Verifying collisions
        game_over = player.intersects(cars)

        if (game_over):
            player.score = 0
            player.level = 1
            player.rect.y = HEIGHT - player.size

            for i in cars:
                i.speed = 0.05

        # Repaint
        pygame.display.update()
        player.lastkey = None


if __name__ == "__main__":
    pygame.init()  # Setup pygame module
    main()
```
