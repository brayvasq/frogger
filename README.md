# Frog Game
Simple Frog game made using Python3 and Pygame library.

| Lenguaje | Versión        | SO                      |
| -------- | -------------- | -----------------       |
| Python   | Python 3.8.2   | Manjaro 20.0.1 Lysia    |


## Documentation

| Type     | Link                                                        |
| -------- | ----------------------------------------------------------- |
| Docs     | https://www.pygame.org/docs/                                |
| Tutorial | https://pythonprogramming.net/pygame-python-3-part-1-intro/ |
| Tutorial | https://realpython.com/pygame-a-primer/                     |

Pygame is a python game development lib, mainly for 2d games. Sprites management oriented and based in SDL 1.2 lib.

## Run project
```bash
# Setup environment
virtualenv .
source bin/activate

# Installing dependencies
pip install -r requirements.txt

# Run project
python src/main.py
```
