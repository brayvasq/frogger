# -*- coding: utf-8 -*-
import pygame
import sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants

class Car(pygame.sprite.Sprite):
    """
    Represent a car or obstacle for frog game
    It's extends from pygame Sprite to represent an Sprite in the game

    ...
    Attributes
    ----------
    n : int
        Is the car Id or number
    speed : float
        Represent the cart velocity
    x : int
        Represent the horizontal position in the screen
    y : int
        Represent the vertical position in the screen
    screen : pygame.display
        It is the screen where the cars will be drawn

    Methods
    -------
    paint()
        Draws the car sprite int the screen.

    move()
        Moves the car sprite and repaint the screen
    """

    def __init__(self, x, y, n, screen):
        """
        Parameters
        ----------
        x : int
            Represent the horizontal position in the screen
        y : int
            Represent the vertical position in the screen
        n : int
            Is the car Id or number. Is the number order in which it was created
        screen : pygame.display
            It is the screen where the cars will be drawn
        """
        pygame.sprite.Sprite.__init__(self)  # Super method
        self.n = n
        self.speed = 0.05  # Car speed
        self.size = 16  # Sprite size
        self.x = x
        self.y = y
        self.screen = screen  # Canvas or screen

        # Object that surround the car's area with a rectangle, used to identify collisions
        self.rect = pygame.Rect(self.x, self.y, self.size, self.size)  # Boundaries

    def paint(self):
        """
        Draws the car sprite int the screen. 
        It draws a blue rectangle to represent the car.
        """
        pygame.draw.rect(self.screen, (61, 90, 254), self.rect)

    def move(self, time):
        """
        Moves the car sprite and repaint the screen

        Parameters
        ----------
        time : clock.tick
            Is the number of frames per second to draw
        """
        if ((self.n-1) % 4 == 0):
            self.rect.x += self.speed * time
            if (self.rect.x + self.size > 400):
                self.rect.x = 0
            self.paint()
        else:
            self.rect.x -= self.speed * time
            if (self.rect.x <= 0):
                self.rect.x = 400
            self.paint()
