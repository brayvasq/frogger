# -*- coding: utf-8 -*-
import pygame, sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants

class Frog(pygame.sprite.Sprite):
    """
    Represent a Frog or Player
    It's extends from pygame Sprite to represent an Sprite in the game

    ...
    Attributes
    ----------
    x : int
        Represent the horizontal position in the screen
    y : int
        Represent the vertical position in the screen
    screen : pygame.display
        It is the screen where the cars will be drawn

    Methods
    -------
    paint()
        Draws the player sprite int the screen.

    horizontal_movement()
        Changes the player sprite horizontal position

    vertical_movement()
        Changes the player sprite horizontal position

    intersects()
        Verify a collision occurs between the player and a car  
    """

    def __init__(self, x, y, screen):
        """
        Parameters
        ----------
        x : int
            Represent the horizontal position in the screen
        y : int
            Represent the vertical position in the screen
        n : int
            Is the car Id or number. Is the number order in which it was created
        screen : pygame.display
            It is the screen where the cars will be drawn
        """
        pygame.sprite.Sprite.__init__(self)
        self.size = 16  # Sprite size
        self.x = x
        self.y = y
        self.score = 0  # Player score
        self.level = 1  # Game level
        self.screen = screen

        # Object that surround the car's area with a rectangle, used to identify collisions
        self.rect = pygame.Rect(self.x, self.y, self.size, self.size)

    def paint(self):
        """
        Draws the player sprite int the screen.
        It draws a red rectangle to represent the player.
        """
        pygame.draw.rect(self.screen, (244, 67, 54), self.rect)

    def horizontal_movement(self, timer, value):
        """
        Changes the player sprite horizontal position

        Parameters
        ----------
        time : clock.tick
            Is the number of frames per second to draw
        """
        self.rect.x += value * timer

        if self.rect.x + self.size > 400:
                self.rect.x = 400 - self.size
        elif self.rect.x < 0:
            self.rect.x = 0

    def vertical_movement(self, value=1):
        """
        Changes the player sprite horizontal position

        Parameters
        ----------
        time : clock.tick
            Is the number of frames per second to draw
        """
        self.rect.y += self.size * 2 * value

        if (self.rect.y <= 0):
            self.rect.y = self.size
        elif self.rect.y + self.size > 640:
            self.rect.y = 640 - self.size

    def intersects(self, targets):
        """
        Verify a collision occurs between the player and a car

        Parameters
        ----------
        targets : list
            Is the list of cars to verify collitions

        Return
        ------
        True
            If a collition occurs
        False
            Otherwise
        """
        val = False
        for i in targets:
            if pygame.sprite.collide_rect(self, i):
                print("Collision !!!!!!!!!!!!!!!!!!")
                val = True

        return val
