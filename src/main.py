#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pygame
import sys  # Pygame and sys modules
from pygame.locals import *  # Import Pygame constants
from frog import Frog
from car import Car

# Constants
WIDTH = 400 # Screen Width
HEIGHT = 640 # Screen height
CENTER = (WIDTH / 2) - (16 / 2) # Screen center

BACKGROUND_COLOR = (255, 249, 196)

# Setup walls
walls = [j for j in range(0, HEIGHT, 32)]  

# Draw walls in the screen
def draw_walls(screen):
    """
    Draws the list of walls.
    It draws a black rectangle to represent the wall, with the full width size.

    Parameters
    ----------
    screen : pygame.display
        It is the screen where the walls will be drawn
    """
    for i in walls:
        rect = pygame.Rect(0, i, WIDTH, 16)
        pygame.draw.rect(screen, (0, 0, 0), rect)


# Draw cars in the screen
def draw_cars(cars):
    """
    Iterate over the cars list and draws each one

    Parameters
    ----------
    cars : list
        List of cars to draw
    """
    for i in cars:
        i.paint()

# Draw game info
def draw_score(screen, player):
    """
    Draws a text to represent the player score

    Parameters
    ----------
    screen : pygame.display
        It is the screen where the walls will be drawn
    player : Frog
        Player instance to obtain the score and level
    """
    myfont = pygame.font.SysFont("monospace", 10)
    label = myfont.render("Score : " + str(player.score) +
                          " Level : " + str(player.level), 1, (255, 255, 0))
    screen.blit(label, (0, 0))


def main():
    """
    Main method that will create the game loop, handle keys inputs
    and draws elements in the screen
    """
    screen = pygame.display.set_mode((WIDTH, HEIGHT))  # Creating a new window
    pygame.display.set_caption("Hello Pygame!!!")  # Window title

    # Initialize vars
    player = Frog(CENTER, HEIGHT - 16, screen)
    clock = pygame.time.Clock()
    pygame.font.init()

    # Setup cars
    cars = []
    for i in range(1, 18, 2):
        print("Creatinf car => ", i)
        car = Car(CENTER, walls[i] + player.size, i, screen)
        cars.append(car)

    ##### Game logic #####
    game_over = False

    # Main loop
    while True:
        time = clock.tick(15)

        keys = pygame.key.get_pressed()

        screen.fill(BACKGROUND_COLOR)

        # Iterate all events
        for event in pygame.event.get():
            # If a event send the close window signal
            if event.type == QUIT:
                pygame.quit()  # Closes pygame module
                sys.exit(0)  # Finish the application
            elif event.type == pygame.KEYUP:
                if keys[pygame.K_RIGHT]:
                    player.horizontal_movement(time, 0.5)
                elif keys[pygame.K_LEFT]:
                    player.horizontal_movement(time, -0.5)
                elif keys[pygame.K_UP]:
                    player.vertical_movement(-1)
                elif keys[pygame.K_DOWN]:
                    player.vertical_movement(1)

        # Background

        # Draw elements
        player.paint()
        # player.move(time, keys, walls)
        draw_walls(screen)
        draw_score(screen, player)

        # Movements
        for i in cars:
            i.move(time)

        # Verify if the player is in the top of the screen to levelup
        # Increase the cars speed and modify the score and level of the player
        if (player.rect.y <= player.size):
            for i in cars:
                i.speed += 0.05
            player.score += 5
            player.level += 1
            player.rect.y = HEIGHT - player.size

        # Verifying collisions
        game_over = player.intersects(cars)

        # Restart game variables if game_over is True
        if (game_over):
            player.score = 0
            player.level = 1
            player.rect.y = HEIGHT - player.size

            for i in cars:
                i.speed = 0.05

        # Repaint
        pygame.display.update()
        player.lastkey = None


if __name__ == "__main__":
    pygame.init()  # Setup pygame module
    main()
